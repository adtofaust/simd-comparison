use std::{arch::x86_64::*, convert::TryInto};
//use ::simd::* as simd_dep;   //needs nightly, does not compile "feature has been removed" as of rust 1.33
//they suggest to use https://crates.io/crates/packed_simd which is also 2 years without update, alos does not compile 

//use core_simd::*;


//we must use seprate fn because main can't be unsafe and target_feature enabled fn must be unsafe
#[target_feature(enable = "avx")]   //here we say that we want to compile this function with avx enabled, it does not overrides whatever is in rustflags
unsafe fn loop_avx() {
    if is_x86_feature_detected!( "avx" ) {   //runtime check, under the hood it also uses cpuid :)
        //union V8S { pub v8s_mm: ManuallyDrop<__256>, pub v8s_f: [f32; 8] }
        let ( mut v8a, mut v8s ) = ( _mm256_setzero_ps(), _mm256_setzero_ps() );
        let arr: [ f32; 1_200_000 ] = [ 23.74; 1_200_000 ];

        for i in arr.iter().step_by(8) {
            v8a = _mm256_loadu_ps( i );
            v8s = _mm256_add_ps( v8s, v8a );
        }

        // faster loop, iter() operates on iterators which are indirection to raw data, chunks() operates on raw data
        arr.chunks(8).for_each(|chunk| {
            v8a = _mm256_loadu_ps( &chunk[0] );   //_mm256_load_ps() operates only on aligned to 256 data, our array doesn't hold this requirement so you will just segfault, that's why we need 'u' version of fn
            v8s = _mm256_add_ps( v8s, v8a );
        } );

        let v8s_sliced = std::mem::transmute::< __m256, [f32; 8] >(v8s);
        let v8s_unpacked: ( f32, f32, f32, f32, f32, f32, f32, f32 ) = std::mem::transmute( v8s );   //bitcasting from __m256 to array, under the hood it's just union
        //dbg!( v8s_sliced );
        //println!( "Test {}, {}, {}, {}, {}, {}, {}, {} ", v8s_unpacked.0, v8s_unpacked.1, v8s_unpacked.2, v8s_unpacked.3, v8s_unpacked.4, v8s_unpacked.5, v8s_unpacked.6, v8s_unpacked.7 );

        //future std::simd example
        // let (mut v8simd, mut sum) = ( f32x8::splat(0.0), f32x8::splat(0.0) );

        // arr.chunks(8).for_each(|chunk| {
        //     v8simd = f32x8::from_array( chunk.try_into().expect("") );
        //     sum += v8simd;
        // });
        // dbg!( sum );

    }
}



fn main() {
    unsafe { loop_avx(); }
}
