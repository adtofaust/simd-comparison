﻿using System;
using System.Numerics;

namespace csharp
{
    class Program
    {
        static void Main(string[] args)
        {
            var lanes = Vector<int>.Count;    //check how wide is SIMD vector (implicitly whther it supports SSE which is 4 lane width, AVX which is 8 lanes and AVX512 which is 16 lanes)
            var vs = new Vector<float>();
            
            if( lanes > 0 )
            {
                float[] arr = new float[1_200_000];
                Array.Fill<float>( arr, 23.74f );

                for( int i = 0; i < arr.Length; i+=lanes )
                {
                    float[] a1 = { arr[0+i], arr[1+i], arr[2+i], arr[3+i], arr[4+i], arr[5+i], arr[6+i], arr[7+i] };
                    var v1 = new Vector<float>( arr, i );
                    vs += v1;
                }

            }
            //Console.WriteLine( "Hello World!" + vs[0] + vs[1] + vs[2] + vs[3] + vs[4] + vs[5] + vs[6] + vs[7] );
        }
    }
}
