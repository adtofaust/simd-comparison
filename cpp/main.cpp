#include <immintrin.h>   //for AVX
#include <iostream>
#include <array>

//remember to add compiler flag "-mavx"

//GCC specific
#include <cpuid.h>

int main()
{
    int info[4];
    __cpuid_count(0, 0, info[0], info[1], info[2], info[3]);
    if( info[0] >= 0x0000'0001 )
    {
        __cpuid_count(0x0000'0001, 0, info[0], info[1], info[2], info[3]);
        if ( info[2] & ((int)1 << 28) != 0 )    //check bit for AVX presence
        {
            __m256 v8a;
            union { __m256 v8s; float f8s[8]; };
            v8a = v8s = _mm256_setzero_ps();

            std::array<float, 1'200'000> arr {};
            arr.fill( 23.74 );

            for( int i = 0; i < arr.size(); i+=8 )
            {
                v8a = _mm256_set_ps( arr[i+7], arr[i+6], arr[i+5], arr[i+4], arr[i+3], arr[i+2], arr[i+1], arr[i+0] );
                v8s = _mm256_add_ps( v8s, v8a );
            }
            
            std::cout << "Test ccc " << f8s[0] << "  " << f8s[1] << "  " << f8s[2] << "  " << f8s[3] << f8s[4] << "  " << f8s[5] << "  " << f8s[6] << "  " << f8s[7] << std::endl;
        }
        
    }




}